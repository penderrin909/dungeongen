# DungeonGen

scan the project root level with Godot and open.

should run just fine by hitting the play button

Things to do:
- [ ] Come up with idea for Dungeon generation
    - [ ] Make pregenerated rooms?
    - [ ] randomly layout rooms?
    - [ ] Totally randomly generated with cavern like layouts and mazes? random loots?
- [ ] Create enemies?
- [ ] What was the goal again?...
- [ ] Make something fun
- [X] Put together a really simple peer to peer network (Godot has functionality built in somwhere but I've never looked at it)
- [ ] Add code to send same level to everyone (Since levels are generated dynamically, the whole level has to be sent over)
    - [ ] Will need some kind of preconfigure stage (see https://docs.godotengine.org/en/stable/tutorials/networking/high_level_multiplayer.html#synchronizing-game-start)
    - [ ] Figure out a way to send the level..
    - [ ] Actually implement usernames. maybe put them above user's heads or something
    - [ ] Create test "enemy" (just a block that HURTS) and add health?

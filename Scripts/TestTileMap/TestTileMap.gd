extends Node2D

# Use this script to create the tilemap objects and set them up

var enemyRes = load("res://Scenes/Entities/Enemy_Test.tscn")

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	spawnEnemies(Vector2(rand_range(0,600), rand_range(0,600)))
	
func spawnEnemies(enemyPosition: Vector2):
	var enemy = enemyRes.instance()
	enemy.global_position = enemyPosition
	add_child(enemy) # Probably should add enemies to a list somewhere...

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

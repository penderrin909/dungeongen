extends TileMap


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var tilesVec = []
enum {UPPER_WALL = 37} # 37 is what I've been testing collision with (Wall)

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	clear() # clear the current tiles
	_generateMap()

func _generateMap() -> void:
	# ========= ADD CODE TO RANDOMLY GENERATE TILES HERE =======================
	var rng = RandomNumberGenerator.new()
	rng.randomize()
	
	for i in range(0,11):
		var newVec2 = null
		while not (newVec2 in tilesVec):
			newVec2 = Vector2(rng.randi_range(-10,10),rng.randi_range(-10,10))
			if not newVec2 in tilesVec:
				tilesVec.append(newVec2)
		
	for tileV in tilesVec:
		set_cell(tileV.x, tileV.y, UPPER_WALL, false, false, false, _get_subtile_coord(UPPER_WALL))
	# create random 10 walls on the 16 by 16 grid 
	# ==========================================================================

func _get_subtile_coord(id):
	# Randomly grabs a tile in an Atlas tileset object
	# (just pick a random tile.. it doesn't currently use tile priority values)
	var tiles = tile_set
	var rect = tiles.tile_get_region(id)
	var x = randi() % int(rect.size.x / tiles.autotile_get_size(id).x)
	var y = randi() % int(rect.size.y / tiles.autotile_get_size(id).y)
	return Vector2(x, y)

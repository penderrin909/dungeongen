extends KinematicBody2D

class_name Entity

var moveSpeed = 100.0
var RunSpeedMultiply = 3.0
var velocity = Vector2(0,0)

puppet var puppet_position = Vector2(0,0) setget puppet_position_set
puppet var puppet_velocity = Vector2()

# All entities NEED a tween object (so movement can be smoothly synced over the network)
onready var tween = $Tween

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	pass

# Overload the get_input function per child class type
# Get input should just modify the entities velocity vector
func get_input(delta: float) -> void:
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	if is_network_master():
		get_input(delta) # get Movement Mods
		move_and_slide(velocity)
	else: # Updating net player
		if not tween.is_active(): # not interpolating, so extrapolate next position
			move_and_slide(puppet_velocity * moveSpeed)

func puppet_position_set(new_value) -> void:
	puppet_position = new_value
	
	tween.interpolate_property(self, "global_position", global_position, puppet_position, 0.1)
	tween.start()

func _on_Network_tick_rate_timeout():
	if is_network_master(): # Send position to everyone else
		rset_unreliable("puppet_position", global_position)
		rset_unreliable("puppet_velocity", velocity)

extends Entity

class_name Player

var playerCam = null # Will create a Camera object

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	# Initialize movement parameters
	moveSpeed = 100.0
	RunSpeedMultiply = 3.0
	velocity = Vector2(0,0)
	
	puppet_velocity = Vector2(0,0)

# Get input should just modify the entities velocity vector
func get_input(delta: float) -> void:
	# Some input tricks to get rid of the if statements for movement
	var x_input = int(Input.is_action_pressed("ui_right")) - int(Input.is_action_pressed("ui_left"))
	var y_input = int(Input.is_action_pressed("ui_down")) - int(Input.is_action_pressed("ui_up"))

	# Could probably pull some tricks to get rid of these if statements but eh
	if Input.is_action_just_pressed("Run"):
		moveSpeed = moveSpeed * RunSpeedMultiply
	if Input.is_action_just_released("Run"):
		moveSpeed = moveSpeed / RunSpeedMultiply
		
	velocity = Vector2(x_input, y_input).normalized() * moveSpeed

func playerCreateCamera() -> void:
	playerCam = Camera2D.new() # we instance scenes, and use new for objects (maybe a duh thought but just thought I'd remind myself...)
	playerCam.make_current()
	add_child(playerCam)

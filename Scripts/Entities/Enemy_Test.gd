extends Entity

class_name Enemy_Test


var angle = 0

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	# Initialize movement parameters
	moveSpeed = 100.0
	RunSpeedMultiply = 3.0
	velocity = Vector2(0,0)
	
	puppet_velocity = Vector2(0,0)

# Get input should just modify the entities velocity vector
func get_input(delta: float) -> void:

	angle += delta # Radians
	var x_input = sin(angle) # Value between 0 and 1
	var y_input = cos(angle) # Value between 0 and 1
	angle = angle if angle < 2*PI else angle-2*PI

	velocity = Vector2(x_input, y_input).normalized() * moveSpeed

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	if get_tree().get_network_unique_id() == 1: # HOST IS 1. Only allow host to control enemies
		get_input(delta) # get Movement Mods
		move_and_slide(velocity)
	else: # Updating net player
		if not tween.is_active(): # not interpolating, so extrapolate next position
			move_and_slide(puppet_velocity * moveSpeed)

func _on_Network_tick_rate_timeout():
	if get_tree().get_network_unique_id() == 1: # Send position to everyone else if host
		rset_unreliable("puppet_position", global_position)
		rset_unreliable("puppet_velocity", velocity)

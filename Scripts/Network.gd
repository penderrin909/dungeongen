extends Node

const DEFAULT_PORT = 7878 # Picked a random port that wasn't used by anything
const MAX_CLIENTS = 10 # This number may change buttt we don't have that many friends so this'll be fine

var server = null
var client = null

var ip_address = ""
var port = DEFAULT_PORT

var player = load("res://Scenes/Entities/Player.tscn")

signal playerCreated

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	# Get network address of self
	for ip in IP.get_local_addresses():
		#print(ip)
		if ip.begins_with("192.168.") and not ip.ends_with(".1"):
			ip_address = ip

	# Setting up signals (SIGNAL connected to FUNCTION)
	get_tree().connect("connected_to_server", self, "_connected_to_server")  # Signals from Network
	get_tree().connect("server_disconnected", self, "_server_disconnected")  # Signals from Network
	
	get_tree().connect("network_peer_connected", self, "_player_connected") # Signals from Network
	get_tree().connect("network_peer_disconnected", self, "_player_disconnected") # Signals from Network
	

# Called from MainMenu/BoxContainer/HBoxContainer/HostButton	_on_Button_pressed
func create_server() -> void:
	server = NetworkedMultiplayerENet.new()
	print("CREATING SERVER ON:\nIP:\t" + str(ip_address) + "\nPORT:\t" + str(port))
	server.create_server(port, MAX_CLIENTS)
	get_tree().set_network_peer(server)
	
func join_server() -> void:
	client = NetworkedMultiplayerENet.new()
	print("CREATING CLIENT ON:\nIP:\t" + str(ip_address) + "\nPORT:\t" + str(port))
	client.create_client(ip_address, port)
	get_tree().set_network_peer(client)
	
func _connect_to_server() -> void:
	print("CONNECTED TO SERVER")
	
func _connected_to_server() -> void:
	print("CONNECTED TO SERVER")
	# Instance myself
	yield(get_tree().create_timer(0.1), "timeout")
	instance_player(get_tree().get_network_unique_id())
	
func _server_disconnected() -> void:
	print("DISCONNECTED FROM SERVER")
	
func _player_connected(id) -> void:
	print("Player " + str(id) + " connected")
	# Instance everyone else
	instance_player(id)

func _player_disconnected(id) -> void:
	print("Player " + str(id) + " disconnected")
	# Remove players when the disconnect
	if Players.has_node(str(id)):
		Players.get_node(str(id)).queue_free()
	
func instance_player(id) -> void:
	# For now everyone joined is placed randomly...
	var player_instance: Player = GlobalHelperFunctions.instance_node_at_location(player, Players, Vector2(rand_range(0,300), rand_range(0,300)))
	player_instance.set_name(str(id))
	player_instance.set_network_master(id)
	# Only create player camera for the user's player, maybe not the most elegant solution butttt it works.
	if player_instance.is_network_master():
		player_instance.playerCreateCamera()
	

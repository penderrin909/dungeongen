extends Button


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Button_pressed():
	# Get all the data we want from the following:
	#	IPLineEdit
	#	PortLineEdit
	#	UsernameLineEdit
	# Find all objects we need (Probably a cleaner way of doing this...)
	var ipText = get_node("/root/MainMenu/VBoxContainer/HBoxContainer/VBoxContainer/IPLayout/IPLineEdit").get_text()
	var portText = get_node("/root/MainMenu/VBoxContainer/HBoxContainer/VBoxContainer/PortLayout/PortLineEdit").get_text()
	var usernameText = get_node("/root/MainMenu/VBoxContainer/UsernameLayout/UsernameLineEdit").get_text()
	print("Connecting...\nIP:\t" + ipText + "\nPort:\t" + portText + "\nUsername:\t" + usernameText)
	
	# Obviously don't go to this TestTileMap scene
	# but launch the client scene (Doesn't exist yet)
	if ipText != "":
		Network.ip_address = ipText
		Network.port = int(portText) #Needs to be an int
		Network.join_server()
		Global.goto_scene("res://Scenes/TestTileMap.tscn")

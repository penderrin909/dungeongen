extends Button


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Button_pressed():
	Network.create_server()
	Network.instance_player(get_tree().get_network_unique_id())
	Global.goto_scene("res://Scenes/TestTileMap.tscn")
